import React from 'react';
import './App.css';
import ReviewItems from './components/Reviews';
import PropTypes from 'prop-types';

function App() {
  return(
    <div>
      <div className="Cardbox">
          <FotoProduk/>
          <InfoProduk isDiskon="yes" name="Messi Barcelona Away 1998" category="POSTER" price="49000"/>
      </div>

      <ReviewItems/>
    </div>
  );
}

function FotoProduk() {
  return (
    <div className="Image">
      <img src="Mockup-messi.jpg" alt=""></img>
    </div>
  );
}

function CekDiskon(props) {
  const { isDiskon, discount } = props;

  if (isDiskon === "yes"){
      return(
        <p>Diskon {discount}% off</p>
      );
  } 
  else if (isDiskon === "soon"){
    return(
      <p>Akan ada diskon soon...</p>
    );
  } 
  else {
    return(
        <p>Belum ada diskon</p>
      );
  }
}

function InfoProduk(props) {
  const { category, price, name, isDiskon } = props;
  const benefits = ["Frame tanpa Kaca", "Bahan Awet Laminasi Doff", "Tersedia Warna Hitam & Putih"];
  const listBenefits = benefits.map((itemBenefits) =>
    <li key={itemBenefits}>{itemBenefits}</li>
  );

  return (
    <div>
      <div className="Description">
        <p className="Cate">{category}</p>
        <h1 className="Title">{name}</h1>
        <p className="Price">IDR {price}</p>
        <CekDiskon isDiskon={isDiskon} discount={50}/>
        <p className="Info">Magna kasd amet stet lorem erat ipsum consetetur. Gubergren ea. Tempor ipsum magna sadipscing dolores dolores accusam sit. Invidunt sanctus.</p>
        <ul>
          {listBenefits}
        </ul>

        <a onClick={(e) => TambahCart(name, e)} href="#">Add to Chart</a>
      </div>
    
    </div>
    
  );
}

function TambahCart(e){
  console.log("Membeli Produk " + e)
}

CekDiskon.propTypes = {
  discount: PropTypes.number.isRequired
};

export default App;
