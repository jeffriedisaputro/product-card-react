import React from 'react';
import '../App.css';


function Reviews() {
    // Dummy Reviews Data JSON
    const users = [
      {
        "id" : 1,
        "pic" : "https://images.pexels.com/photos/2100063/pexels-photo-2100063.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "name" : "Laurent Sai",
        "review" : "Mantab, Harga terjangkau kualitas bukan kaleng-kaleng"
      },
      {
        "id" : 2,
        "pic" : "https://images.pexels.com/photos/1115697/pexels-photo-1115697.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        "name" : "Magdaline",
        "review" : "Woke Thankyou, Harga terjangkau kualitas bagus bangetz"
      },
      {
        "id" : 3,
        "pic" : "https://images.pexels.com/photos/935969/pexels-photo-935969.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        "name" : "Lurah Uganda",
        "review" : "Mantab Djiwa, Terimakasih Bosque.."
      }
    ];
  
    const listReviews = users.map((itemReview) =>
      <div key={itemReview.id} className="Item">
        <img src={itemReview.pic}></img>
        <div className="User">
            <h3>{itemReview.name}</h3>
            <p>{itemReview.review}</p>
        </div>
      </div>
    );
  
    return (
      <div className="Review-box">
        <h2>Reviews</h2>
        {listReviews}
      </div>
    );
  }
  
export default Reviews;